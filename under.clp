

;Added by Meena(19.3.10)
;He was convicted under an obscure 1990 law . 
(defrule under0
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 law)
(kriyA-under_saMbanXI  ? ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_wahawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp     under0   "  ?id "  ke_wahawa )" crlf))
)




(defrule under1
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) law)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_aMwargawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under1   "  ?id "  ke_aMwargawa )" crlf))
)



(defrule under2
(declare (salience 4900))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_nIce))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under2   "  ?id "  ke_nIce )" crlf))
)

;ADDED BY PRACHI RATHORE

(defrule under3
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) pressure|pudding|Art)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under3   "  ?id "  me )" crlf))
)

;"under","Prep","1.ke_nIce/ke_wale/se_kama/ke_aXIna"
;He is doing his doctorate under Prof.Mohan.
;


;;@@@   ---;ADDED BY PRACHI RATHORE
;Elements heavier than iron are also made inside stars [under] special conditions.[gyannidhi]
;  असाधारण स्थिति में लोहा की अपेक्षा भारी तत्व  भी तारों के अन्दर  बनाए गये हैं . 
(defrule under4
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-under_saMbanXI  ? ?id1)
(id-root ?id1 condition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under4   "  ?id "  meM )" crlf))
)

;;@@@   ---;ADDED BY PRACHI RATHORE
;But the fact is that for a while Gangotri was under the rule of the Gurkhas.[gyannidhi]
;  बात यह थी कि गंगोत्री कुछ समय के लिए गोरख राज्‍य के अधीन हो गयी थी।
(defrule under5
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(kriyA-under_saMbanXI  ? ?id1)
(id-root ?id1 rule)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_aXIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under5   "  ?id "  ke_aXIna)" crlf))
)
