
(defrule instant0
(declare (salience 5000))
(id-root ?id instant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wawkAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  instant.clp 	instant0   "  ?id "  wawkAla )" crlf))
)

;;$$$   ---modified by Prachi Rathore
;Meaning changed from Asanna to kRaNa
(defrule instant1
(declare (salience 4900))
(id-root ?id instant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRaNa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  instant.clp 	instant1   "  ?id "  kRaNa )" crlf))
)

;default_sense && category=noun	kRaNa	0
;"instant","N","1.kRaNa"
;Come here this instant.
;
;

;;@@@   ---Added by Prachi Rathore
;There's instant tea too, isn't there, Daddy?[gyannidhi]
;बनी बनाई चाय भी तो होती है, है न डैडी?
(defrule instant2
(declare (salience 5500))
(id-root ?id instant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 coffee|tea|pudding|rice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banI_banAyI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  instant.clp 	instant2   "  ?id "  banI_banAyI )" crlf))
)
