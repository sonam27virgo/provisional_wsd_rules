;##############################################################################
;#  Copyright (C) 2002-2005 Garima Singh (gsingh.nik@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;I agree with her analysis of the situation.
;He agreed with them about the need for change.
(defrule agree0
(declare (salience 5000))
(id-root ?id agree)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahamawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  agree.clp 	agree0   "  ?id "  sahamawa )" crlf))
)

(defrule agree1
(declare (salience 4000))
(id-root ?id agree)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahamawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  agree.clp 	agree1   "  ?id "  sahamawa )" crlf))
)

(defrule agree2
(declare (salience 5500))
(id-root ?id agree)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 figure|account)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mela_KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  agree.clp 	agree2   "  ?id "  mela_KA )" crlf))
)


;******************DEFAULT RULE*********************************

(defrule agree3
(declare (salience 0))
(id-root ?id agree)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahamawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  agree.clp 	agree3   "  ?id "  sahamawa )" crlf))
)


