
(defrule shake0
(declare (salience 5000))
(id-root ?id shake)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-down_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id biCO));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " shake.clp shake0 " ?id "  biCO )" crlf)) 
)

(defrule shake1
(declare (salience 4900))
(id-root ?id shake)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 biCO))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shake.clp	shake1  "  ?id "  " ?id1 "  biCO  )" crlf))
)

(defrule shake2
(declare (salience 4800))
(id-root ?id shake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shake.clp 	shake2   "  ?id "  hilA )" crlf))
)

(defrule shake3
(declare (salience 4700))
(id-root ?id shake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hilAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shake.clp 	shake3   "  ?id "  hilAnA )" crlf))
)
;Added by jagriti(23.11.2013)
;People in southern California were shaken awake by an earthquake.
;दक्षिणी कैलिफोर्निया के निवासियों को भूकम्प के झटकों ने जगा दिया .
(defrule shake4
(declare (salience 5001))
(id-root ?id shake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 awake)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jagA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shake.clp 	shake4   "  ?id "  jagA_xe )" crlf))
)
;Added by jagriti(23.11.2013)
;Anna shook some powdered chocolate over her coffee.
;एना ने अपनी कॉफ़ी में  थोडा सा चाकलेट का पाउडर मिलाया .
(defrule shake5
(declare (salience 5002))
(id-root ?id shake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 chocolate|salt|suger)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shake.clp 	shake5   "  ?id "   mila )" crlf))
)
;Her voice shook as she spoke about the person who attacked her.
;उसकी आवाज भय से कांपने लगी जब उसने उस पर हमला करने वाले के बारे में बताया .
(defrule shake6
(declare (salience 5003))
(id-root ?id shake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 voice)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAMpa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shake.clp 	shake6   "  ?id "   kAMpa )" crlf))
)

;"shake","N","1.hilAnA"
;Give the jar a good shake.
;--"2.spanxana"
;--"3.kRaNa"
;Hang on! I'll be ready in two shakes.
;
;
