;##############################################################################
;#  Copyright (C) 2013-2014 Anita Chaturvedi (anita@iiit.ac.in)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;She stood by the door to receive her guests as they arrived. [cambridge.org/dictionary/learner-;english]
;अतिथियों के आगमन पर स्वागत के लिए वह दरवाजे पर खड़ी थी  ।
;The prime minister's speech was well/warmly/coldly, etc. received by the conference delegates.
;[cambridge.org/dictionary/learner-english]
;प्रधानमंत्री के भाषण का कांफ्रेंस के प्रतिनिधियों ने गर्मजोशी से स्वागत किया  ।
(defrule receive1
(declare (salience 10))
(id-root ?id receive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
;(kriyA-subject  ?id ?)
(id-root ?id1 guest|minister)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAgawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receive.clp 	receive1   "?id "  svAgawa_kara )" crlf))
)
;He was received into the church. [cambridge.org/dictionary/learner-english]
;उसको चर्च में शामिल किया गया  ।
(defrule receive2
(declare (salience 15))
(id-root ?id receive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?)
(kriyA-karma  ?id ?)
(kriyA-into_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAmila_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receive.clp 	receive2   "?id "  SAmila_kara )" crlf))
)
;#####################defaultrule################################
;Did you receive my letter? [cambridge.org/dictionary/learner-english]
;क्या आपने मेरा पत्र प्राप्त किया ?[verified-sentence]
;I received a phone call from your mother. [cambridge.org/dictionary/learner-english]
;तुम्हारी माँ से मुझे फ़ोन कॉल प्राप्त हुई थी ।
;Members of Parliament received a 4.2% pay increase this year. [cambridge.;org/dictionary/learner-english]
;पार्लियामेंट के सदस्यों को इस वर्ष ४.२ % की बढोत्तरी प्राप्त हुई ।
;She died after receiving a blow to the head. [cambridge.org/dictionary/learner-english]
;सिर पर चोट लगने के बाद वह मृत्यु को प्राप्त हुआ ।
;I'm receiving you loud and clear . [cambridge.org/dictionary/learner-english]
;मैं तुम्हारी आवाज़ बिल्कुल स्पष्ट और ज़ोर से प्राप्त कर रहा हूँ ।
(defrule receive0
;(declare (salience 10))
(id-root ?id receive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receive.clp 	receive0   "?id "  prApwa_kara )" crlf))
)
