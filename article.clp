;##############################################################################
;#  Copyright (C) 2002-2005 Garima Singh (gsingh.nik@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

; She wrote an article attacking the judges and their conduct of the trial.
;उसने जजों कीं पैरवी के ढ़ंग कीं आलोचना करते हुए एक लेख लिखा
(defrule article0
(declare (salience 3500))
(id-root ?id article)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 write|read|discuss|publish|described)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id leKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  article.clp 	article0   "  ?id "  leKa )" crlf))
)

;*******************DEFAULT RULES**************************************


(defrule article1
(declare (salience 3500))
(id-root ?id article)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 write|read|discuss|publish|described)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu
))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  article.clp 	article1   "  ?id "  vaswu )" crlf))
)

;********************************************************* EXAMPLES **************************************************************************
; She wrote an article attacking the judges and their conduct of the trial.

