;---------------------------------------- call as a noun 
;---------------------------------------------------------------
; old wsd rule .
;Teaching is considered an apt calling for women.
;टीचिंग महिलाओं के लिए एक उचित व्यवसाय माना जाता है ।
;old wsd rule
(defrule call0
(declare (salience 4800))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id calling )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vyavasAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  call.clp  call0 "  ?id "  vyavasAya )" crlf))
)


;Added Anita(03-07-13)-hinkhoj dictionary
;All Gods are given call through MANTRAS during yagya.
;यज्ञ के दौरान सारे ईश्वरों का मन्त्रों के द्वारा अवाहन किया गया.
(defrule call1
(declare (salience 4700))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?kri ?id)
(kriyA-through_saMbanXI ?kri ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvAhana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp  call1 "  ?id "  AvAhana )" crlf))
)
;Added Anita
;There was a disputed call in the second set.
;दूसरे समुच्चय में एक विवादकर निर्णय था ।
;Added Anita
(defrule call2
(declare (salience 4700))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirNaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp  call2 "  ?id " nirNaya )" crlf))
)

;He felt the call of the priesthood early on in his life. 
;उसने अपने जीवन की शुरुआत में ही पुरोहित बनने की अन्तर्प्रेरणा को महसूस किया.
;Added Anita
(defrule call3
(declare (salience 4700))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
(kriyA-object ?kri ?id)
(id-root ?kri feel)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) anwarpreraNA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp  call3 "  ?id " anwarpreraNA)" crlf))
)

;This is the last call for passengers travelling on British Airways flight 199 to Rome.
;यह रोम के लिए ब्रिटिश एअरवेज़ की उड़ान 199 से यात्रा करने वाले यात्रियों के लिए आखिरी पुकार  है ।
;last_call word added in multi_word expression.txt
;I heard a distinctive call of the cuckoo.
;मैंने कोयल की एक विशेष पुकार सुनी ।
;Added Anita
(defrule call4
(declare (salience 3000))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (viSeRya-of_saMbanXI  ?id  ?)(viSeRya-for_saMbanXI ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pukAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp  call4 "  ?id "  pukAra )" crlf))
)

;During all this time, I never once felt the call of nature, nor did I suffer from hunger.
;Added Anita
;(defrule call_of1
;(declare (salience 4850))
;(id-root ?id call)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) of)
;(viSeRya-of_saMbanXI  ?id ?)
;(id-word =(+ ?id 2) nature)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1)=(+ ?id 2) SOcAxi_kI_AvaSyakawA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_of1 "  ?id "  " =(+ ?id 1) " "=(+ ?id 2)" SOcAxi_kI_AvaSyakawA)" crlf))
;)

;Added Anita
;(defrule call4
;(declare (salience 4600))
;(id-root ?id call)
;?mng <-(meaning_to_be_decided ?id)
;(id-root =(+ ?id 1) of)
;;(viSeRya-of_saMbanXI  ?id ?id1)
;(kriyA-object  ?id ?)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id upayoga))
;(if ?*debug_flag* then
;(printout wsd_fp"(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " call.clp  call11 "  ?id "    upayoga )" crlf))
;)





;---------------------------------  call as a verb (compound mng) ---------------------------------------------
;Meaning from old wsd rule .
;modified by Anita(old wsd sentence)
;She called me up yesterday morning for the party. (parse wrong)
;उसने पार्टी के लिए कल सुबह मुझे टेलिफोन किया ।
(defrule call_up1
(declare (salience 3560))
(id-root ?id call)
(id-word ?id called)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?obj)
(id-root ?obj  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(or (kriyA-kriyA_viSeRaNa ?id ?id1)(kriyA-upasarga ?id ?id1)(kriyA-subject  ?id ?)(kriyA-for_saMbanXI  ?id ?)
)
(id-word ?id1 up)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 teliPona_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng " ?*prov_dir* " call.clp  call_up1  " ?id ?id1 "    teliPona_kara  )" crlf))
)

;Added Anita-oxford dictionary
;The smell of the sea called up memories of her childhood.
;समुद्र की गंध ने उसके बचपन की स्मृतियों को स्मरण कराया ।
(defrule call_up2
(declare (salience 4200))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 smaraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_up2  " ?id "  " ?id1 "  smaraNa_kara  )" crlf))
)

;Added Anita-oxford dictionary
;She called up her last reserves of strength.
;उसने अपनी अवशिष्ट शक्ति को जुटाया ।
(defrule call_up3
(declare (salience 4300))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 up)
(kriyA-object ?id ?obj)
(viSeRya-viSeRaNa  ?obj ?id2)
(id-root ?id2 last)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 jutA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* " call.clp  call_up3  " ?id "  " ?id1"  "?id2" jutA)" crlf))
)

;Added Anita-oxford dictionary
;I called his address up on the computer.
;मैंने उसका पता सङ्गणक से लेकर मालुम किया ।
;safgaNaka para ke Upara usakA pawA upayoga kiyA.
(defrule call_up4
(declare (salience 4400))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word  ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(test (neq ?id1 (+ ?id 1)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1  mAluma_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_up4 "  ?id "  " ?id1 " mAluma_kara )" crlf))
)

;Added Anita
;I will call up again later.
;मैं फिर से बाद में टेलिफोन करूँगा ।
(defrule call_up5
(declare (salience 4100))
(id-root ?id call)
(id-word =(+ ?id 1) up)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) teliPona_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_up5 "  ?id "  " =(+ ?id 1) "  teliPona_kara )" crlf))
)

;Added Anita-oxford dictionary 
;I'm  waiting for someone to call me back with a price.
;मैं मूल्य के साथ किसी के वापस फोन करने की प्रतीक्षा कर रहा हूँ ।
(defrule call_back
(declare (salience 4400))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id vApasa_teliPona_kara))
(if ?*debug_flag* then
(printout wsd_fp"(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_back  " ?id " " ?id1 " vApasa_teliPona_kara )" crlf))
)

;Added Anita-oxford dictionary
;He called out a warning from the kitchen. 
;उसने रसोईघर से चेतावनी दी ।
(defrule call_out
(declare (salience 4400))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-object  ?id ?obj)
(id-root ?obj warning)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_out "  ?id "  " ?id1 "  xe)" crlf))
)

;Added Anita
;Parents called out to their kids, took their hands or lifted them into their arms.
;माँ बाप ने उनके बच्चो को आवाज दी, उनके हाथ पकडे या उनको गोदी में उठा लिया.
;" Mr. Lindbergh, a picture for the papers? " called out a reporter.
;Mr. Lindbergh, अखबार के लिये चित्र ?" संवाददाता ने आवाज लगायी
(defrule call_out1
(declare (salience 4300))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 out)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AvAjza_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_out1 "  ?id "  " ?id1 "  AvAjza_xe)" crlf))
)

;Meaning from old wsd rule .
;The classes are being called off.
;कक्षा रद्द की जा रहीं हैं ।
(defrule call_off
(declare (salience 4200))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)

=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 raxxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_off  "  ?id "  " ?id1 "  raxxa_kara  )" crlf))
)
;Added Anita
;Please call your dog off .
;कृपया अपने कुत्ते को वापस बुलाओ ।
(defrule call_off1
(declare (salience 4300))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-object ?id ?obj)
(id-cat_coarse ?id verb)
(id-root ?obj  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vApasa_bulA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp call_off1  "  ?id "  " ?id1 "  vApasa_bulA  )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  call.clp  call_off1   "  ?id " ko )" crlf)
)

;Added Anita
;Call in the doctor,patient is in critical condition.
;डॉक्टर को बुलाओ, मरीज़ की स्थिति गम्भीर है।
(defrule call_in
(declare (salience 4300))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) in)
(kriyA-in_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1)  bulAo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_in "  ?id "  "(+ ?id 1) "  bulAo )" crlf))
)

;Added Anita
;A new team of detectives were called in to conduct a fresh inquiry.
;जासूसों की एक नई टीम को नए सिरे से जांच का संचालन करने में सहायता करने के लिए बुलाया गया ।
(defrule call_in1
(declare (salience 4100))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 in)
(id-word =(+ ?id 2) to)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) sahAyawA_ke_lie_bulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_in1 "  ?id "  " =(+ ?id 1) "  sahAyawA_ke_lie_bulA )" crlf))
)

;Added Anita-oxford dictionary
;Cars with serious faults have been called in by the manufacturers.
;गंभीर कमियों वाली कारों को निर्माताओं द्वारा वापस  ले लिया गया ।'
(defrule call_in2
(declare (salience 4100))
(id-root ?id call )
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) in)
(id-word =(+ ?id 2) by)
;(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) vApasa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_in2 "  ?id "  " (+ ?id 1) "  vApasa_le )" crlf))
)

;Added Anita-oxford dictionary
;His speech called forth an angry response.
;उनके भाषण ने एक नाराज़गी की प्रतिक्रिया उत्पन्न की ।
(defrule call_forth
(declare (salience 4100))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-upasarga ?id ?)
(id-word =(+ ?id 1) forth)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) uwpanna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_forth "  ?id "  " =(+ ?id 1) "  uwpanna_kara  )" crlf))
)

;Added Anita-oxford dictionary
;I'll call round and see you on my way home.
;मैं भेंट करने के लिए बुलाऊँगा और घर आते हुए तुमसे मार्ग में मिलूँगा ।
(defrule call_round
(declare (salience 4100))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?obj)
(id-root ?obj round)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?obj BeMta_karane_ke_lie_bulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_round  " ?id "  " ?obj "  BeMta_karane_ke_lie_bulA)" crlf))
)

;Added Anita-own generated sentence 
;I called for the bill in the restaurant.
;मैंने भोजनालय में बिल मँगवाया .
(defrule call_for1
(declare (salience 4100))
(id-root ?id call )
(id-word =(+ ?id 1) for)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-for_saMbanXI ?id ?)
(kriyA-in_saMbanXI ?id ?sam)
(id-root ?sam  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) mazgavA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_for1 "  ?id "  " (+ ?id 1) "  mazgavA )" crlf))
)

;Meaning from old wsd rule .
;modified by Anita-oxford dictionary
;The opposition have called for him to resign. 
;विपक्ष ने उससे त्याग-पत्र देने की माँग की ।
;They called for the immediate release of the hostages.
;उन्होंने बन्धक व्यक्तियों की तात्कालिक रिहाई की माँग की ।
;The situation calls for prompt action. 
;स्थिति तुरन्त कार्यवाही की माँग करती है।
(defrule call_for2
(declare (salience 4000))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) for)
(kriyA-subject  ?id ?id1)
(id-word ?id1 opposition|situation|they)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) mAzga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_for2 "  ?id "  " =(+ ?id 1) "  mAzga_kara  )" crlf))
)


;Added Anita-oxford dictionary
;I now call upon the chairman to address the meeting.
;मैं अब बैठक को सम्बोधित करने के लिए अध्यक्ष को आमन्त्रित करता हूँ ।
(defrule call_upon
(declare (salience 4100))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) upon)
(kriyA-upon_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) AmaMwriwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp  "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi " ?*prov_dir* " call.clp call_upon  "  ?id "  "(+ ?id 1)"  AmaMwriwa_kara)" crlf)
(printout wsd_fp  "(kriyA_id-object_viBakwi " ?id " ko)" crlf))
)

;Added Anita-dante dictionary
;Currently in the UK generic medicines may be  called by two different names .
;आजकल यूके में सामान्य औषधियों को दो अलग-अलग नामों से जाना जा सकता है ।
(defrule call_by
(declare (salience 4500))
(id-root ?id call)
(id-word ?id called)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-upasarga ?id ?)
(id-word =(+ ?id 1) by)
(kriyA-by_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) jAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp  call_by "  ?id "  " =(+ ?id 1) "   jAnA  )" crlf))
)

;--------------------------- call as a verb (single meaning) ----------------------------------------------------------
;Added Anita-oxford dictionary
;This train calls at Didcot and Reading.
;यह रेलगाड़ी डिडकोट और रीडिंग में रुकती है ।
(defrule call5
(declare (salience 3360))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 train|bus)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ruka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " call.clp  call5 "  ?id "  ruka  )" crlf))
)

;Meaning from old wsd rule .
;modified by Anita-cambridge dictionary
;It's the sort of work that calls for a high level of concentration.
;यह ऐसा कार्य है जिसमें उच्च स्तर की एकाग्रत्ता की आवश्यकता होती है ।
(defrule call6
(declare (salience 3400))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?sam)
(viSeRya-of_saMbanXI  ?sam ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvaSyakawA_ho))
(if ?*debug_flag* then
(printout wsd_fp"(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " call.clp	call6  "  ?id " AvaSyakawA_ho  )" crlf))
)

;Meaning from old wsd rule .
;modified by Anita-oxford dictionary
;I called him many times.
;मैंने उसको बहुत बार बुलाया।
;I'll call a taxi for you.
;मैं आपके लिए टैक्सी बुलाऊँगा ।
;Did somebody call my name?
;क्या किसी ने मेरा नाम बुलाया ? 
(defrule call7
(declare (salience 3550))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?)
(kriyA-object  ?id ?obj)
;(kriyA-for_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp   call7   "  ?id "  bulA )" crlf))
)

;Added Anita-oxford dictionary
;This calls for a celebration.
;इसके लिए उत्सव होना चाहिए ।
(defrule call8
(declare (salience 3500))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?id1)
(id-root ?id1 celebration)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " call.clp	call8  "  ?id " ho)" crlf))
)

;Added Anita-(03-07-13)-oxford dictionary
;They decided to call the baby Mark. 
;उन्होंने शिशु को मारक् नाम देने के लिए निश्चय किया ।
;They decided to call their first daughter after her grandmother.
;उन्होंने अपनी प्रथम बेटी को दादी का नाम  दिया ।
(defrule call9
(declare (salience 3660))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
;(to-infinitive  ?to ?id)
(or (kriyA-subject  ?id ?)(kriyA-object ?id ?obj)(kriyA-object_2  ?id ?obj)(kriyA-after_saMbanXI  ?id ?)
)
(id-root ?obj son|daughter|mother|father)
(id-cat_coarse ?id verb)
;(id-cat_coarse ?obj PropN|noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAma_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp  call9  "  ?id " nAma_xe )" crlf))
)

;Meaning from old wsd rule .
;modified by Anita
;I'm supposed to call you after twenty-four hours.
;मुझे आपको चौबीस घंटों के बाद टेलिफोन करना हैं .
(defrule call10
(declare (salience 3625))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?obj)
(kriyA-subject  ?id ?)
(kriyA-after_saMbanXI  ?id ?)
(kriyA-kriyArWa_kriyA  ? ?id)
(id-root ?obj  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id teliPona_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " call.clp  call10  " ?id "    teliPona_kara  )" crlf))
)
;parser problem
;Meaning from old wsd rule .
;modified by Anita
;When she was done, father began to call our names one after the other.
;जब उसने खत्म कर लिया, पिताजी ने हम लोगों के नाम एक के बाद एक पुकारना शुरू कर दिये.
(defrule call11
(declare (salience 3665))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-word ?obj  names)
;(to-infinitive ? ?id)
(or(kriyA-after_saMbanXI ?id ?)(kriyA-object ?id ?obj)(kriyA-subject  ?id ?)(kriyA-kriyArWa_kriyA  ? ?id))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pukAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " call.clp  call11  " ?id "    pukAra )" crlf))
)

;Meaning from old wsd rule .
;modified by Anita-oxford dictionary
;Are you calling me a liar?
;क्या आप मुझे झूठा कह रहे हैं?
;In this sentece word order is not correct & 'kyA' word is also not coming .
;What do they call that new fabric. 
;वे इस नई संरचना को क्या कहते हैँ ?
;He was in the front room, or the lounge or whatever you want to call it.
;वह सामने के कमरे में था, या लॉन्ज में या आप इसे जो भी कहना चाहें .
;I would not call German an easy language.
;मैं जर्मन भाषा को आसान नहीं कहूँगा ।
(defrule call12
(declare (salience 3345))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or (kriyA-object_2  ?id ?)(kriyA-kriyA_niReXaka ?id ?id1)(kriyA-kqxanwa_karma ?id ?)(kriyA-object_1  ?id ?)(kriyA-object  ?id ?)(kriyA-subject  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp 	call12  "  ?id "  kaha )" crlf))
)

;(defrule call11
;(declare (salience 3100))
;(id-root ?id call)
;?mng <-(meaning_to_be_decided ?id)
;(to-infinitive  ?to ?id)
;(kriyA_object ?id ?obj)
;(id-cat_coarse ?obj propN)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id nAma_xe))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp  call11  "  ?id " nAma_xe )" crlf))
;)

;Meaning from old wsd rule .
;modified by Anita
;They called me names.
;उन्होंने मेरे नाम पुकारे. (or उन्होंने मुझे गाली दी )
;John was punished for calling his teacher names.
;जॉन को अपने शिक्षक को गाली देने के लिए दंड दिया गया ।
;Billy cried when the other kids called him names.
;बिली ज़ोर से रोया जब दूसरे बच्चों ने उसको गाली दी ।
(defrule call13
(declare (salience 3666))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-subject  ?id ?)
(or (kriyA-object  ?id ?id1)(and (kriyA-subject  ?id ?id2)(kriyA-kqxanwa_karma ?id ?id1)(kriyA-for_saMbanXI  ? ?id)))
;(kriyA-object  ?id ?)
(id-word ?id1 names)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1 gAlI_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " call.clp      call13  "  ?id "  " ?id1 "  gAlI_xe  )" crlf))
)
;------------------------------------  Default rules -------------------------------------------------------
;old wsd rule .
;She is a busy woman with many calls on her time.
;वह अपने समय की बुलावों के साथ एक व्यस्त स्त्री है .
;The doctor has five calls to make this morning.
;डॉक्टर को आज सुबह पाँच बुलावे करने हैं ।
(defrule call_default_noun
(declare (salience 100))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bulAvA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp  call_default_noun "  ?id "  bulAvA )" crlf))
)

;Meaning is from old wsd rule but in parser call is not added as Adjective .
;Modified by Anita-dante dictionary 
;I thought I'd like to try a good singer , a guy called Kelvin Blacklock .
;मैंने सोचा कि मैं कैलविन ब्लैकलॉक नामक एक अच्छे गायक को आज़माना चाहता हूँ  ।
(defrule call31
(declare (salience 1900))
(id-root ?id call)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  call.clp     call31   "  ?id "  nAmaka )" crlf))
)

;Added Anita-dante dictionary
;Currently in the UK generic medicines may be called by two different names .

;I just thought i would call by on my way into town.
;मैंने सोचा कि शहर जाते समय उधर से होते हुए जाऊँगा ।



;---------------------------------------- summary -------------------------------------
; call --- noun
;	1. vyavasAya
;	2. AvAhana
;	3. nirNaya
;	4. aMwarpreraNa
;	5. pukAra

;call -- verb
;   call_up	1. teliPona_kara
;		2. mAmula_kara
;		3. juTA
;		4. smarNa_kara

;   call_back 	1. vApasa_teliPona_kara

;   call_out 	1. xe 
;		2. pukAra

;   call_off 	1. raxxa_kara
;		2. vApasa_bulA

;   call_in 	1. bulAo

; When she was done, father began to call our names one after the other.   ===> call = pukAra

