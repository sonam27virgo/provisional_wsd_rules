
;Modified by Prachi Rathore(21-10-13)
;Meaning changed from aXika to UzcA and also salience is reduced.
 
;Modified by Meena(3.3.11) ; added (viSeRya-viSeRaNa ?id1 ?id) and deleted (samAsa ?id2 ?id)
;Added by Meena(4.12.09)
;High income taxes are important .

;A high building.[CAMBRIDGE learnersdictionary]
;एक ऊँची इमारत . 
(defrule high0
(declare (salience 4700))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(or(samAsa ?id2 ?id1)(viSeRya-viSeRaNa ?id2 ?id)(viSeRya-viSeRaNa ?id1 ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp      high0   "  ?id "  uzcA )" crlf))
)

;Salience reduced by Meena(4.12.09)
(defrule high1
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high1   "  ?id "  UzcA )" crlf))
)

; Changed from paxa_meM_UzcA to UzcA : Amba
;"high","Adj","1.paxa_meM_UzcA"
;jilAXISa kA paxa jile meM sabase'high' howA hE.
;
(defrule high2
(declare (salience 4900))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcAI_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high2   "  ?id "  UzcAI_para )" crlf))
)

;"high","Adv","1.UzcAI_para"
;bADZa se bacane ke liye loga'high' Gara banAwe hE'


;Salience reduced by Meena(3.12.09)
(defrule high3
(declare (salience 0))
;(declare (salience 4800))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcA_sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high3   "  ?id "  UzcA_sWAna )" crlf))
)

;"high","N","1.UzcA_sWAna"
;Sera bAjAra Ajakala'high' nahIM hE.
;Added by Meena
;It is high time we updated our thinking on women 's issues .
(defrule high4
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
(id-root =(+ ?id 1) time|noon)
(viSeRya-viSeRaNa  =(+ ?id 1)  ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp    high4   "  ?id "  TIka )" crlf))
)



;Added by Prachi Rathore(21-10-13)

;Avoid foods that are high in salt.[CAMBRIDGE learnersdictionary]
;आहार को टालिए जिनमें नमक ज्यादा हैं . 
(defrule high5
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-in_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high5   "  ?id "  jyAxA )" crlf))
)

;;Added by Prachi Rathore(21-10-13)
;The whole band seemed to be high on heroin.[CAMBRIDGE learnersdictionary]
;पूरा बैंड मादक पदार्थ पर धुत होता है प्रतीत हुआ .

(defrule high6
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-on_saMbanXI ?id ?id1)
(id-root ?id1 heroin|cocain|drink|drug)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high6   "  ?id "  Xuwa )" crlf))
)

;Added by Prachi Rathore(21-10-13)

;The coach is very high on this new player. [M-W learnersdictionary]
;प्रशिक्षक इस नये खिलाडी पर अत्यन्त उतेजित है . 
(defrule high7
(declare (salience 4900))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-on_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwejiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high7   "  ?id "  uwejiwa )" crlf))
)

;;@@@   ---Added by Prachi Rathore
;The ball looped high up in the air.[oald]
;गोले ने वायु मेँ ऊँचाई पर फन्दा बनाया . 
(defrule high8
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA_viSeRaNa-kriyA_viSeRaNa_viSeRaka ?id1 ?id)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 UzcAI_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " high.clp	high8  "  ?id "  " ?id1 " UzcAI_para  )" crlf))
)

