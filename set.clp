
(defrule set0
(declare (salience 5000))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id setting )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jamAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  set.clp  	set0   "  ?id "  jamAnA )" crlf))
)

;given_word=setting && word_category=noun	$vAwAvaraNa)

;"setting","N","1.vAwAvaraNa"
;The golf field is situated in perfect setting.
;--"2.vinyAsa"
;The cooker has several temperature settings.
;--"3.jamAvata"
;Can you give me your diamond ring with gold setting.
;--"4.saMgIwa"
;Rehaman's setting for vandemataram is very popular.
;
;
(defrule set1
(declare (salience 4900))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 free)
(kriyA-free_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukwa_kiyA_gayA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " set.clp set1 " ?id "  mukwa_kiyA_gayA )" crlf)) 
)

(defrule set2
(declare (salience 4800))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 free)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mukwa_kiyA_gayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set2  "  ?id "  " ?id1 "  mukwa_kiyA_gayA  )" crlf))
)

(defrule set3
(declare (salience 4700))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_ho));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " set.clp set3 " ?id "  wEyAra_ho )" crlf)) 
)
;$$$ Modified by jagriti(12.12.2013)..........nikala_paZanA in place of wEyAra_honA
;The expedition set out for the upper reaches of the Amazon.
;अभियान दल अमेज़न के उच्च विस्तार के लिए निकल पड़े.
(defrule set4
(declare (salience 4600))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nikala_padZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir*" set.clp	set4  "  ?id "  " ?id1 "  nikala_padZa  )" crlf))
)

(defrule set5
(declare (salience 4500))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWApiwa_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " set.clp set5 " ?id "  sWApiwa_kara )" crlf)) 
)

(defrule set6
(declare (salience 2400))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sWApiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set6  "  ?id "  " ?id1 "  sWApiwa_kara  )" crlf))
)
;@@@ Added by jagriti(26.11.2013)
;Our house is set back from the road.[cald][veena mam trans]
;हमारा घर सडक से दूर स्थित है.
(defrule set7
(declare (salience 4300))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xUra_sWiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set7  "  ?id "  " ?id1 "  xUra_sWiwa_ho  )" crlf))
)

(defrule set8
(declare (salience 4200))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 trap)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set8   "  ?id "  raKa )" crlf))
)
;Added by jagriti(26.11.2013)
;It is believed that the building was set on fire deliberately.[cambridge advance learner][veena mam translation]
;ऐसा कहा जाता है कि इमारत में आग जानबुझकर लगाई गयी थी .

(defrule set9
(declare (salience 4100))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 fire)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set9   "  ?id "  lagA )" crlf))
)

;@@@ Added by jagriti(26.11.2013)
;If I've made a mistake, then it's up to me to set it right (= correct it) .[veena mam trans]
; अगर मैंने कोई गलती की है तो यह मेरे पर निर्भर है कि उसे सही करूं .
(defrule set10
(declare (salience 4000))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kqxanwa_karma  ?id ?id1)
(id-root ?id1 right)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set10   "  ?id "  sahi_kara )" crlf))
)
;@@@ Added by jagriti(26.11.2013)
;The government has set new limits on spending.[veena mam]
;सरकार ने खर्च करने पर नयी सीमा स्थापित की है .
;Lewis has set a new world record .
;लेविस ने नया वर्ड रिकार्ड स्थापित किया है.
;The court's decision has set a legal precedent .
;न्यायालय के निर्णय ने एक नया उदाहरण स्थापित किया .
;Parents should set a good example to their children.
;माता –पिता को बच्चों के लिए एक अच्छा उदाहरण स्थापित करना चाहिए .

(defrule set11
(declare (salience 3900))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 limit|record|example|precedent)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWApiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set11   "  ?id "  sWApiwa_kara )" crlf))
)
;@@@ Added by jagriti(26.11.2013)
;He set the alarm for 7.00 a.m.[veena mam]
;उसने सुबह सात बजे का अलार्म लगाया .
;Will you set the table (= put plates and utensils on it ready for use) , please?
;क्या आप कृपया मेंज पर खाने के लिए बर्तन लगा देंगे ?
(defrule set12
(declare (salience 3800))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 alarm|tray|table)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set12   "  ?id "  lagA )" crlf))
)

;@@@ Added by jagriti(26.11.2013)
;During the interval the stage was set for the second act.[veena mam]
;मध्यांतर के समय  दूसरे अंक के नाटक के लिए मंच तैयार किया गया . 

(defrule set13
(declare (salience 3700))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 stage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set13   "  ?id "   wEyAra_kara )" crlf))
)
;@@@ Added by jagriti(26.11.2013)
;Has a date/time been set for the meeting yet?[veena mam]
;क्या अभी तक मीटिंग के लिए तारीख और समय निर्धारित किया जा चुका है ?
;The price of the house has been set at £425 000.
;घर के लिए ४२५००० डालर तय किए गये है .
(defrule set14
(declare (salience 3600))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or (kriyA-for_saMbanXI  ?id ?id1) (kriyA-at_saMbanXI  ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirXAriwa_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set14   "  ?id "   nirXAriwa_kara )" crlf))
)
;@@@ Added by jagriti(26.11.2013)
;A gold tiepin with a diamond set into it.[veena mam]
;सोने के टाई पिन में हीरा जड़ा है
;A brooch set with rubies and pearls. (जड़ी हैं )
(defrule set15
(declare (salience 3200))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or (kriyA-into_saMbanXI  ?id ?id1) (kriyA-with_saMbanXI  ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jadZ_xe ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set15   "  ?id "   jadZ_xe )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;When a doctor sets a broken bone, he or she puts it into a fixed position so that it will heal .[veena mam]
; जब डाक्टर टूटी हुई हड्डी को जोड़ता है ,तब वह उसे निर्धारित ठीक स्थान पर रखता है ताकि वह ठीक हो सके .`
; When a broken bone sets, it heals in a fixed position.
; जब टूटी हुई हड्डी जुड़ जाती है ,तो अपनी जगह पर ठीक हो जाती है .
(defrule set16
(declare (salience 3400))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-object ?id ?id1)(kriyA-subject ?id ?id1))
(id-root ?id1 bone)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodZa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set16   "  ?id "   jodZa )" crlf))
)

;@@@ Added by jagriti(27.11.2013)
;He set a vase of flowers on the table.[veena mam]
;उसने मेंज पर एक फूलों का गुलदस्ता रक्खा .
;The campsite is set in the middle of a pine forest.
;कैम्प लगाने की  जगह को पाइन के जंगल के बीच रक्खा गया है .
(defrule set17
(declare (salience 3300))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or (kriyA-in_saMbanXI  ?id ?id1) (kriyA-on_saMbanXI  ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set17   "  ?id "   raKa )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;Her attacker set about her with a knife.[veena mam]
; उसके हमलावर ने चाकू से उस पर हमला किया (वार किया ).
(defrule set18
(declare (salience 3500))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 about)
(kriyA-about_saMbanXI ?id ?id1)
(kriyA-subject ?id ?id2)
(id-root ?id2 attacker)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 hamalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set18  "  ?id "  " ?id1 "  hamalA_kara  )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;I tried to apologize, but I think I set about it the wrong way. [veena mam]
; मैंने माफी मांगने का प्रयास किया ,पर लगता है मैंने गलत तरीके से पेशकश  की .
(defrule set19
(declare (salience 3100))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 peSakaSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set18  "  ?id "  " ?id1 "  peSakaSa_kara  )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;This war has set neighbour against neighbour.[veena mam]
; इस लड़ाई ने पड़ोसी को पड़ोसी से लडवा दिया .
(defrule set20
(declare (salience 3000))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-against_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id3)
(id-root ?id3 war)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ladZvA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set20   "  ?id "  ladZvA  )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;The cost of business travel and entertainment can be set against tax .[veena mam]
; व्यवसायिक यात्रा और मनोरंजन व्यवसाय की कीमत दोनों को मिलाकर करों को कम किया जा सकता है .
(defrule set21
(declare (salience 2900))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(kriyA-against_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id1 tax)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set22   "  ?id "  kama_kara  )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;You have to set the advantages of the scheme against the disadvantages.[veena mam]
; तुम्हें स्कीम के नुकसान के सामने उसके फायदे (लाभ ) की तुलना करनी पड़ेगी . 
(defrule set22
(declare (salience 2800))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-against_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wulanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set22   "  ?id "  wulanA_kara  )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;The Court of Appeal set aside his conviction.
;न्यायालय ने उसकी अपील पर अपना निर्णय सुरक्षित रक्खा. 
(defrule set23
(declare (salience 2700))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 aside )
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-word ?id2 conviction)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 surakRiwa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set23  "  ?id "  " ?id1 "  surakRiwa_raKa  )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;He had some money in an account that he'd set aside for his kids.
;उसके खाते में कुछ पैसे थे जो उसने अपने बच्चों के लिए अलग रक्खें थे . [veena mam]
; I set aside half an hour every evening to hear Erik read.
;एरिक ने क्या पढ़ा यह सुनने के लिए मैंने हर शाम आधा घंटे का समय रक्खा .
(defrule set24
(declare (salience 2600))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 aside )
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 alaga_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set24  "  ?id "  " ?id1 "  alaga_raKa  )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;The market was full of brightly coloured vegetables set out on stalls.[veena mam]
;बाजार में सारे स्टाल्स को रंगविरंगें रंगों की सब्जियों से सजाया गया है . 
(defrule set25
(declare (salience 4600))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set25  "  ?id "  " ?id1 "  sajA  )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;She set herself up as an interior designer.[veena mam]
; उसने अपने को एक आंतर डिज़ाइन करने वाली के रूप में प्रतिष्ठित किया .
(defrule set26
(declare (salience 4400))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 prawiRTiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set26  "  ?id "  " ?id1 "  prawiRTiwa_kara  )" crlf))
)
;@@@ Added by jagriti(27.11.2013)
;I need one or two people to help me set up the equipment.[veena mam]
;उपकरण लगाने के लिए मुझे एक या दो लोंगो की सहायता की जरूरत होगी .
(defrule set27
(declare (salience 5000))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2 equipment)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set27  "  ?id "  " ?id1 "  lagA  )" crlf))
)
;Added by jagriti(28.11.2013)
;She plans to set up her own business.[veena mam]
;उसने अपना कारोबार शुरू करने का इरादा किया है .
(defrule set28
(declare (salience 5001))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ?id2 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SurU_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set28  "  ?id "  " ?id1 " SurU_kara )" crlf))
)
;Added by jagriti(28.11.2013)
;We only had a couple of hours to set up before the exhibition opened.[veena mam]
;नुमाइश खुलने से पहले हमारे पास तैयारी के लिए केवल कुछ घंटे ही बाकी थे .
(defrule set29
(declare (salience 5001))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-samakAlika_kriyA  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wEyArI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set29  "  ?id "  " ?id1 " wEyArI_kara )" crlf))
)
;Added by jagriti(28.11.2013)
;Winning the lottery has set them up for life .[veena mam]
;  लाटरी की जीत ने उनको जीवन भर की ( के लिए) सुविधाएँ दे दी है .
(defrule set30
(declare (salience 5001))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-vAkya_subject ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 suviXAeM_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " set.clp	set30  "  ?id "  " ?id1 " suviXAeM_xe )" crlf))
)
(defrule set31
(declare (salience 2300))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set31  "  ?id "  niSciwa )" crlf))
)
;Added by jagriti(26.11.2013)
;A set of prime numbers.[iit-bombay] 
; मुख्य अङ्कों का समूह. 
(defrule set32
(declare (salience 2200))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samUha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set32   "  ?id " samUha )" crlf))
)
Modified by jagriti(28.11.2013)
(defrule set33
(declare (salience 2100))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set33 "  ?id " xe)" crlf))
)
;@@@@
;Added by jagriti(11.12.2013)
;I've already told you that when travelling in the mountains, one must set off at the crack of dawn.[gyanidhi-corpus]
;मैं आपको पहले से ही बता चुका हूँ कि पर्वतों में यात्रा करते समय  हर एक को उषाकाल की दरार से दूर रवाना होना चाहिए. .
(defrule set34
(declare (salience 5000))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 off)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ravAnA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set34 "  ?id " ravAnA_ho)" crlf))
)

;@@@ Added by jagriti(13.12.2013)
;I set up a goat as bait, but the leopard refused to be tempted.
;मैंने शिकार के लिए एक बकरा भी बांध रखा था किन्‍तु चीते ने उसकी ओर देखा तक नहीं।
(defrule set35
(declare (salience 5500))
(id-root ?id set)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 up)
(kriyA-object ?id ?id2)
(id-root ?id2 goat|cow)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAMzXa_raKa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  set.clp 	set35 "  ?id " bAMzXa_raKa)" crlf))
)

;"set","Adj","1.niSciwa"
;This Hotel serves meals at set times.
;--"2.xqDZa"
;She has very set ideas on womens' issues. 
;--"3.kqwrima"
;She always has a set expression on her face.
;--"4.wEyAra"
;The new political party looks set for victory in the general election.
;--"5.nirXAriwa"
;Every winter they visit us on set days.
;
;LEVEL 
;Headword : set
;
;Examples --
;
;"set","N","1.seta" (eka sAWa kAma meM AnevAlI vaswuoM kA samUha jEse 'hAra, aMgUTI, bunxe Ora kadZe' milakara eka gahanoM kA 'seta' hogA.)
;Can you get a cutlery set for me?
;kyA wuma mere liye eka katalarI sEta lAoge. (katalarI seta - kAztA, cammaca Ora CurI)
;--"2.xqSyabanXa"
;The film has beautiful sets.
;Pilma meM sunxara xqSyabanXa hEM.
;--"3.samuccaya"
;He is solving the problems in the chapter sets.
;vaha aXyAya samuccayoM kI samasyAoM ko sulaJA rahA hE.
;
;"set","V","1.raKa xenA"
;She set a tray down on the table.
;usane mejZa para tre raKa xI. 
;--"2.CuAyA"
;Hari set pen to paper to complete the letter.
;hari ne pawra Kawama karane ko kAgajZa para kalama CuAyA.
;--"3.lagAnA"
;Why don't you set the alarm at 6 O' clock?
;wuma CaH baje kA alArma kyoM nahIM lagAwe ho ?
;--"4.jadZanA"
;Her necklace was set with rubies.
;usakA hAra rUbiyoM se jadZA WA.
;--"5.niSciwa karanA"
;They haven't set a date for their wedding yet.
;unhoMne aBI waka apanI SAxI kA xina niSciwa nahIM kiyA hE.
;--"6.kAyama karanA"
;She set a new world record for the marathon.
;usane mErAWana xOdZa meM eka nayA viSva rikOYrda kAyama kiyA.
;--"7.jamanA"
;The cement will take some time to set.
;sImeMta ko jamane meM kuCa samaya legA.
;--"8.kasanA"
;She set her jaw in a determined manner.
;usane apanI TodZI ko xqDZawA se kasa liyA
;--"9.bETAnA"
;The sergeon set her broken bone.
;sarjana ne usakI tUtI haddI ko biTAyA.
;--"10.kampojZa karanA"
;Books were previously set by hand but not any more.
;pahale kiwAboM ko hAWa se kampojZa kiyA jAwA WA, para aba nahIM.
;--"11.aswa ho jAnA"
;The sun in northern countries sets much later in summer than in winter.
;uwwarI xeSoM meM sUraja garmiyoM meM sarxiyoM kI apekRA xera se aswa howA hE.
;
;"set","Adj","1.niSciwa"
;Hotel meals are served at set times.
;hotala meM KAnA niSciwa samaya para xiyA jAwA hE.
;--"2.xqDZa"
;She has very set ideas on bringing up children.
;baccoM ko pAlane ke viRaya meM usake bahuwa xqDZa vicAra hEM.
;--"3.eka_hI_waraha_kA"
;She always has a set expression on her face.
;usake cehare para hameSA eka hI sA BAva rahawA hE.
;--"4.wEyAra"
;The new political party looks set for victory in the general election.
;nayA rAjanEwika xala Ama cunAvoM meM jIwa ke liye wEyAra lagawA hE. 
;--"5.nirXAriwa"
;Every winter they visit us on set days.
;ve hara sarxiyoM meM hamAre pAsa nirXAriwa xinoM meM Awe hEM.
;
;
;Upara xiye uxAharaNoM se 'set' ke kuCa arWa Upara nikala kara Awe hEM.
;'bETAnA','jadZanA','CuAnA','jamAnA','raKanA' Ora 'aswa_honA' Axi arWa paraspara sambanXiwa lagawe hEM.
;
;anwarnihiwa_sUwra ;
;
;			bETAnA=jamAnA
;                          |
;                |---------|------------|
;                |         |            |
;	(nIce_kI_ora_jAnA) xqDZa_banAnA   (bETAne kA sWAna)
;                |         |            |
;                |         |           (sWAna para)
;            aswa_honA   niSciwa_hojAnA |----|------|
;                                    |    |      |
;                                  raKanA  CuAnA  jadZanA
;                                    |
;                                    |
;                           (eka_sAWa_kAma_AnevAlI_cIjZoM_ko_eka_sWAna_para raKanA)
;                                    | 
;                              (EsI_cIjZoM_kA_samUha)
;                                    |
;                                   seta
;
;sUwra : jamAnA^seta         
